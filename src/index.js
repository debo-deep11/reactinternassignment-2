import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import './index.css';
import App from './components/App';
import JobDetails from './components/JobDetails';
import reducers from './Reducers';



ReactDOM.render(
  <Provider store={createStore(reducers)}>
    {/* {console.log(reducers)} */}
    <App />
    <JobDetails />
  </Provider>,
  document.getElementById('root')
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
