import { connect } from "react-redux";
import react from "react";
import CompanyList from "./CompanyList"

const CompanyListDetails = (props) =>
{
    
    if(!props){
       return <div>Loading...</div>
    }
    else {
        console.log("else working",props)
    return (
        
    <div className="CompanyList" >
        <h3>{props.Title}</h3>
        <h5>{props.Salary}</h5>
        <h5>{props.Location}</h5>
        <h5>{props.Type}</h5>
        <h6>{props.Requirement}</h6>

    </div>   
    )
    }
}

const mapStateToProps = (state)=>{

    return { SelectCompany : state.SelectedCompany}
}


export default connect(mapStateToProps)(CompanyListDetails);