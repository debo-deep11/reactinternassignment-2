import React from "react";



//JobDetails class wiil be used for calling jobBrief componenets

class JobBrief extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isLoading: true }
    }
     
    componentDidMount() {
        setTimeout(() => {
            this.setState({ isLoading: false });
        }, 2000)
    }


    render() {
    
        if (this.state.isLoading) {
            return <div>Loading...</div>
        }
        
        else {
            return (
                <div className="JobBrief" onClick={()=>{this.props.doneClick(this.props)}}>
                    <h1>{this.props.Name}</h1>
                    <h3>{this.props.City},{this.props.Country} </h3>
                    <img src={this.props.Logo} />
                    <p>{this.props.Description}</p>
                    <p>Salary : {this.props.Salary}</p>
                    {this.props.Index }

                </div>
            )
              
            
        }
        
    }


}

export default JobBrief