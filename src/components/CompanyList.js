import React,{Component } from "react";
import { connect } from "react-redux";
import { SelectCompany } from "../Action/actions"
import CompanyListDetails from "./CompanyListDetails"


class CompanyList extends Component 
 {


 RenderList =() => {
  
     return this.props.companies.map((Company) => {
       return (
            <div>  
            <h3 onClick ={()=> {this.props.SelectCompany(Company)}} >{Company.name}</h3>
            
            </div>
      )
   })
     
 }

 MapJobList =() => 
 { 
  if(!this.props.SelectedCompany.jobs)
  { 
   console.log("COming from MapJobList",this.props);
   
    return <div>Select One</div>
 
  }
  else
  { 
    return ( 

    this.props.SelectedCompany.jobs.map((Cjob)=>{
     return(  
   <CompanyListDetails 
   Title ={Cjob.title}
   Salary ={Cjob.salary}
   Location ={Cjob.locations}
   Type ={Cjob.jobType}
   Requirement ={Cjob.requirements}
   />
   )
     }

    )
    )
 }

}

TitleMap =() => {
  if(!this.props.SelectedCompany.jobs)
  {
    return<div></div>
  }
  else{
    
    return <h4>{this.props.SelectedCompany.name}</h4>
  }
}
   

render()
  { 
    
         return ( 
          <>
          <div className="JobOption">{this.RenderList()}</div>
          <div className="TitleBox">{this.TitleMap()}</div>
          <div>{this.MapJobList()}</div>
          </>
        )
        
   }

 }



const mapStateToProps = (state) => {
  console.log(state.SelectedCompany);
  return { companies : state.CompanyList.companies,
           SelectedCompany : state.SelectedCompany}
}

export default connect(mapStateToProps, {SelectCompany})(CompanyList);