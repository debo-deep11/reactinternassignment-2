import React from "react";

const JobDetails =({Job}) => {

  if (!Job) {
    return <div ></div>
  }

  else
  {
  return(

          <div className="JobSingleDetails" >
          <h1>{Job.Name}</h1>
          <h3>{Job.City},{Job.Country}</h3>
          <img src={Job.Logo} />
          <p>{Job.Description}</p>
          <p>Salary : {Job.Salary}</p>
          {Job.Index }

            </div>
  ) 
  }

}

export default JobDetails