import { React } from "react";
import Jobs from "./jobs.json";
import JobBrief from "./jobBrief";



const JobBriefList = ({doneClick}) => { 
 
  
  return (
    <>
      {

        Jobs.map((job, index,) => {

          return (
            <JobBrief  key={index} 
             Name={job.name}
              City={job.location.city}
              Country={job.location.country}
              Logo={job.logo}
              Description={job.description}
              Salary={job.salary}
              doneClick={doneClick }
              
             />)
        })

      }
    </>
  )

    }



export default JobBriefList;