import React from "react";
import './App.css';
import Jobs from "./jobs.json";
import SearchBarForLocation from "./SearchBarforLocation";
import SearchBarForName from "./SearchBarForName";
import JobDetails from "./JobDetails";
import JobBriefList from "./JobBriefList";
import Profile from "./Profile";
import CompanyList from "./CompanyList";
import Route from "./Route";
import Header from "./Header";


class App extends React.Component {

  state = {selectedJob: null};

 onNamesSubmit = term => {

 };


doneClick = (Job) => {
  this.setState({ selectedJob: Job})
  console.log(Job);
}

  render() {
    const showJobList = () => {
      if(window.location.pathname === '/') {

    return (
      <>
       <div className="FlexGame">
         <SearchBarForName onPropNameSubmit={this.onNamesSubmit}/>
         <SearchBarForLocation />
          <a 
              className="btn btn-primary"
              href="/profile"
              target="_blank">
                See Your Profile
            </a>
            <a 
              className="btn btn-primary"
              href="/companies"
              target="_blank">
                Companies
            </a>

         </div>
         
       <div className="gridBox">
         <div className="JobColumn">
           <JobBriefList doneClick={this.doneClick} />
         </div>
         <JobDetails Job={this.state.selectedJob}/>
       </div>
      </>
    )

      }
    }


  
  return (
    <div >
      <Header />
      {showJobList()}
      <Route path="/profile">
        <Profile />
      </Route>
      <Route path="/companies">
        <CompanyList />
      </Route>
        
    </div>
  );


  }



}



export default App;
