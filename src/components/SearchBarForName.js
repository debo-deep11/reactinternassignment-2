import React from "react";

class SearchBarForName extends React.Component
{
    state = { term: '' };

    onNameChange = event => {
        this.setState({ term: event.target.value });
    }

    onFormNameSubmit = event => {
        event.preventDefault();
        this.props.onFormNameSubmit(this.state.term);
    }

    render()
    {
        return( 
          <div>
              <form onSubmit={this.onFormNameSubmit} >
                <label htmlFor="seNm">Search By Name : </label>
              <input type="text" placeHolder="Search by Name" Name="SeNm" value={this.state.term} onChange={this.onNameChange} />
              </form> 
          </div>
      )

    }

}

export default SearchBarForName;