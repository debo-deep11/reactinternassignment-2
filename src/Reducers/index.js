import Companies from './Companies.json';
import { combineReducers } from 'redux';
import CompanyList from '../components/CompanyList';


const CompanyListReducer = () =>
{
    // console.log(Companies)
    return Companies
}

const SelectedCompanyReducer = (SelectedCompany = 'null' , action) => {

    if( action.type === 'SELECTED_COMPANY')
    {
        return action.payload
      
    }

    return SelectedCompany

}


export default combineReducers({
    CompanyList: CompanyListReducer,
    SelectedCompany: SelectedCompanyReducer

});